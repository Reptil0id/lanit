import allure_commons
import allure
from allure_commons.types import AttachmentType
from allure_commons.utils import now, uuid4
from allure_commons.reporter import AllureReporter
from allure_commons.logger import AllureFileLogger
from allure_commons.model2 import Status
from allure_commons.model2 import TestResult
from allure_commons.model2 import TestStepResult

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import pytest
import time


class TestDODO:

    # Перед началом теста
    def setup_class(self):
        self.driver = webdriver.Chrome()

    # После теста
    def teardown_class(self):
        self.driver.quit()

    # Перед каждым тестом
    def setup_method(self, method):
        self.driver.get("https://dodopizza.ru/izhevsk")

    @allure.feature('Заполнение заявки на работу')
    @allure.severity('normal')
    def test_job(self):

        # Выбор места
        def job(menu,search, text):
            self.driver.implicitly_wait(10)
            elem = self.driver.find_element_by_xpath(menu)
            self.driver.implicitly_wait(10)
            time.sleep(1)
            elem.click()
            elem = self.driver.find_element_by_xpath(search)
            elem.send_keys(text)
            elem.send_keys(Keys.ENTER)

        # Заполнение контактных данных
        def info(menu, text):
            elem = self.driver.find_element_by_xpath(menu)
            elem.send_keys(text)

        # Переход по ссылке
        continue_link = self.driver.find_element_by_link_text('Работа в Додо')
        continue_link.click()
        self.driver.implicitly_wait(10)
        self.driver.switch_to.window(self.driver.window_handles[1])

        with allure.step('Выбор места'):
            # Выбираем город
            job("//div[@class='hero__form-item form-block-with-label']", "//input[@placeholder='Поиск']", "Ижевск")
            # Выбираем адрес работы
            job("//div[@id='partialUnits']", "//input[@placeholder='Поиск']", 'ТРЦ "Италмас" (ул. Автозаводская, 2А)')
            # Выбираем вакансию
            self.driver.implicitly_wait(10)
            element = self.driver.find_element_by_xpath("//div[@id ='partialVacancies']")
            time.sleep(1)
            element.click()
            element = self.driver.find_element_by_xpath("//*[@id='partialVacancies']/span[1]/span[1]/span")
            element.send_keys(Keys.ENTER)
        with allure.step('Заполняем контактные данные'):
            # Заполняем контактные данные
            info("//*[@id='name']", "Умер")
            info("//*[@id='lastname']", "Мужик")
            info("//*[@id='birthday']", "66666666")
            info("//*[@id='phone']", "6666666666")
            info("//*[@id='SocialLink']", "-_-")
            # Ставим галочку
            element = self.driver.find_element_by_xpath("//*[@id='form']/div[7]/label")
            element.click()
            allure.attach(self.driver.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)

    # Проверка корзины и выбора пиццы
    @allure.feature('Проверка корзины и выбора пиццы')
    @allure.severity('normal')
    def test_pizza(self):
        # Кусок xpath кода для покупки
        button = "/div/div[4]/div[3]/div[2]/button"

        # Покупка пиццы
        def pizza(size, thickness, buy):
            element = self.driver.find_element_by_xpath(size)
            element.click()
            self.driver.implicitly_wait(10)
            element = self.driver.find_element_by_xpath(thickness)
            element.click()
            self.driver.implicitly_wait(10)
            element = self.driver.find_element_by_xpath(buy)
            element.send_keys(Keys.ENTER)
            self.driver.implicitly_wait(10)
        with allure.step('Покупка пиццы'):
            # Чизбургер-пицца
            pizza("//label[@for='size_20_3']", "//label[@for='dough_20_2']","//*[@id='pizzas']/div/div[1]/div/div[1]" + button)
            # Крэйзи пепперони
            pizza("//label[@for='size_163_2']", "//label[@for='dough_163_1']","//*[@id='pizzas']/div/div[1]/div/div[2]" + button)
            # Мексиканская
            pizza("//label[@for='size_10_1']", "//label[@for='dough_10_2']","//*[@id='pizzas']/div/div[3]/div/div[2]" + button)
            allure.attach(self.driver.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)
        pizzas = ["Чизбургер-пицца", "Крэйзи пепперони", "Мексиканская"]
        with allure.step('Переход в корзину'):
            # Переход в корзину
            link = self.driver.find_element_by_xpath("//button[@class='Button-sc-91ilwk-0 styled__CartButton-sc-1bwqahw-2 kIQPgA']")
            link.click()
            self.driver.implicitly_wait(10)
            self.driver.switch_to.window(self.driver.window_handles[1])
            allure.attach(self.driver.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)

        with allure.step('Проверка корзины'):
            product = self.driver.find_elements_by_xpath("//div[@class='cart__line-name']")
            i = 0
            for prod in product:
                assert pizzas[i] in prod.text
                i += 1